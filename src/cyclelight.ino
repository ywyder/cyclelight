#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
  #include <avr/power.h>
#endif

#define PIN 5
#define NUMLED 36

// Parameter 1 = number of pixels in strip
// Parameter 2 = Arduino pin number (most are valid)
// Parameter 3 = pixel type flags, add together as needed:
//   NEO_KHZ800  800 KHz bitstream (most NeoPixel products w/WS2812 LEDs)
//   NEO_KHZ400  400 KHz (classic 'v1' (not v2) FLORA pixels, WS2811 drivers)
//   NEO_GRB     Pixels are wired for GRB bitstream (most NeoPixel products)
//   NEO_RGB     Pixels are wired for RGB bitstream (v1 FLORA pixels, not v2)
//   NEO_RGBW    Pixels are wired for RGBW bitstream (NeoPixel RGBW products)
Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMLED, PIN, NEO_GRB + NEO_KHZ800);


// color definition
const uint32_t COLORDARKESTRED = 0x050000;
const uint32_t COLORDARKRED = 0x100000;
const uint32_t COLORMEDIUMKRED = 0x800000;
const uint32_t COLORBRIGHTRED = 0xCC0000;
const uint32_t COLORBLACK = 0x000000;
const uint32_t PULSEFREQUENCE = 5;

// color brightness definition
const uint16_t MinimumBrightness = 1; // value between 1-255
const uint16_t BrightnessFactor =  1.8; // value between 0 - 2.55

// duration of animations
const uint16_t DurationKnightRider = 2000;
const uint16_t DurationSmoothBlink = 30000;
const uint16_t DurationLedAnimation = DurationKnightRider + DurationSmoothBlink;
long startAnimation = millis(); // inital timereference for duration calculation

// IMPORTANT: To reduce NeoPixel burnout risk, add 1000 uF capacitor across
// pixel power leads, add 300 - 500 Ohm resistor on first pixel's data input
// and minimize distance between Arduino and first pixel.  Avoid connecting
// on a live circuit...if you must, connect GND first.

void setup() {
  strip.begin();
  strip.show(); // Initialize all pixels to 'off'
}

int pos = 0, dir = 1; // Position, direction of "eye"
int mode = 0; // setting inital mode of animation

void loop() {
  // first slot for animation
  if (millis() - startAnimation <= DurationKnightRider){
    wipe();
    knightrider();
  }
  else if (millis() - startAnimation > DurationKnightRider && millis() - startAnimation <= DurationLedAnimation){
    wipe();
    smoothBlink();
  }
  else {
    startAnimation = millis();
  }
}

void wipe(){
  int m;
  for(m = 0; m <= NUMLED; m++) strip.setPixelColor(m, COLORBLACK);
  strip.show();
}

void smoothBlink(){
  int n; // iterator for all LEDs
  int t; // iterator for pulsating LEDs

  // set all leds to red
  for(n = 0; n <= NUMLED; n++) strip.setPixelColor(n, COLORBRIGHTRED);
  // pulse variing brightest
  for(t = 0; t < 100; t++){
    strip.setBrightness(max(MinimumBrightness, BrightnessFactor*t));
    strip.show();
    delay(PULSEFREQUENCE);
  }
  strip.show();
  for(t = 100; t > 0; t=t-1){
    strip.setBrightness(max(MinimumBrightness, BrightnessFactor*t));
    strip.show();
    delay(PULSEFREQUENCE);
  }

}

void knightrider(){

  int j = 3;
  // Draw 5 pixels centered on pos.  setPixelColor() will clip any
  // pixels off the ends of the strip, we don't need to watch for that.
  strip.setPixelColor(pos - 4, COLORDARKESTRED); // even darker red
  strip.setPixelColor(pos - 3, COLORDARKRED); // Dark red
  strip.setPixelColor(pos - 2, COLORDARKRED); // Dark red
  strip.setPixelColor(pos - 1, COLORMEDIUMKRED); // Medium red
  strip.setPixelColor(pos, COLORBRIGHTRED); // Center pixel is brightest
  strip.setPixelColor(pos + 1, COLORMEDIUMKRED); // Medium red
  strip.setPixelColor(pos + 2, COLORDARKRED); // Dark red
  strip.setPixelColor(pos + 3, COLORDARKRED); // Dark red
  strip.setPixelColor(pos + 3, COLORDARKESTRED); // even darker red

  strip.setBrightness(100*BrightnessFactor);
  strip.show();
  delay(30);

  // Rather than being sneaky and erasing just the tail pixel,
  // it's easier to erase it all and draw a new one next time.
  for(j=-4; j<= 4; j++) strip.setPixelColor(pos+j, 0);

  // Bounce off ends of strip
  pos += dir;
  if(pos < 0) {
    pos = 1;
    dir = -dir;
  } else if(pos >= strip.numPixels()) {
    pos = strip.numPixels() - 4;
    dir = -dir;
  }
}
